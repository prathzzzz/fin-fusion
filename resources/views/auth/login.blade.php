@extends('app')


@section('main-content')
    <div class="row flex-grow">
        <div class="col-lg-5 mx-auto">
            <div class="auth-form-light text-left p-5">
                <div class="brand-logo">
                    <img src="{{ asset('assets/images/logo.svg') }}" alt="Brand Logo">
                </div>
                <h4>{{ __('Hello! Let\'s get started') }}</h4>
                <h6 class="font-weight-light">{{ __('Sign in to continue.') }}</h6>
                <form class="pt-3" method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group">
                        <input type="email" name="email" class="form-control form-control-lg" id="email"
                            value="{{ old('email') }}" required autofocus autocomplete="username"
                            placeholder="{{ __('Email') }}">
                        @if ($errors->has('email'))
                            <span class="text-danger">{{ $errors->first('email') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control form-control-lg" id="password" required
                            autocomplete="current-password" placeholder="{{ __('Password') }}">
                        @if ($errors->has('password'))
                            <span class="text-danger">{{ $errors->first('password') }}</span>
                        @endif
                    </div>
                    <div class="mt-3 d-grid gap-2">
                        <button class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn"
                            type="submit">{{ __('Sign In') }}</button>
                    </div>
                    <div class="my-2 d-flex justify-content-between align-items-center">
                        <div class="form-check">
                            <label class="form-check-label text-muted">
                                <input type="checkbox" class="form-check-input" name="remember"
                                    {{ old('remember') ? 'checked' : '' }}>
                                {{ __('Remember me') }}
                            </label>
                        </div>
                        @if (Route::has('password.request'))
                            <a href="{{ route('password.request') }}"
                                class="auth-link text-primary">{{ __('Forgot your password?') }}</a>
                        @endif
                    </div>
                    <div class="text-center mt-4 font-weight-light">
                        {{ _('Don\'t have an account?') }} <a href="{{ route('register') }}"
                            class="text-primary">{{ _('Create') }}</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
