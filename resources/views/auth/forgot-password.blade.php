@extends('app')


@section('main-content')
    <div class="row flex-grow">
        <div class="col-lg-5 mx-auto">
            <div class="auth-form-light p-5 shadow-sm">

                <h4 class="text-center mb-3">Forgot Your Password?</h4>
                <p class="text-center mb-4 text-gray-600 dark:text-gray-400">
                    Just let us know your email address and we will email you a password reset link.
                </p>

                <!-- Session Status -->
                @if (session('status'))
                    <div class="alert alert-success mb-4" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <form method="POST" action="{{ route('password.email') }}">
                    @csrf

                    <!-- Email Address -->
                    <div class="form-group mb-3">
                        <label for="email" class="form-label">Email</label>
                        <input type="email" class="form-control form-control-lg" id="email" name="email"
                            placeholder="Email" value="{{ old('email') }}" required autofocus>
                        @error('email')
                            <div class="text-danger mt-2">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="d-grid gap-2">
                        <button type="submit" class="btn btn-gradient-primary btn-lg">Email Password Reset Link</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
