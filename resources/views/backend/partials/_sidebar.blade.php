<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" data-bs-toggle="collapse" href="#all-users" aria-expanded="false" aria-controls="all-users">
                <span class="menu-title">All Users</span>
                <i class="menu-arrow"></i>
                <i class="mdi mdi-account-group menu-icon"></i>

            </a>
            <div class="collapse" id="all-users">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="../pages/ui-features/typography.html">Display Leaderboard</a>
                    </li>
                </ul>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-bs-toggle="collapse" href="#chore-management" aria-expanded="false" aria-controls="chore-management">
                <span class="menu-title">Chore Management</span>
                <i class="menu-arrow"></i>
                <i class="mdi mdi-file-tree menu-icon"></i>

            </a>
            <div class="collapse" id="chore-management">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="../pages/ui-features/typography.html">Upload Daily Chores</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../pages/ui-features/typography.html">Upload Articles</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../pages/ui-features/typography.html">Upload Quiz</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../pages/ui-features/typography.html">Upload Videos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../pages/ui-features/typography.html">View Chores</a>
                    </li>
                </ul>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="../docs/documentation.html" target="_blank">
                <span class="menu-title">Review Submissions</span>
                <i class="mdi mdi-check-bold menu-icon"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="../docs/documentation.html" target="_blank">
                <span class="menu-title">Answer Queries</span>
                <i class="mdi mdi-frequently-asked-questions menu-icon"></i>
            </a>
        </li>
    </ul>
</nav>
