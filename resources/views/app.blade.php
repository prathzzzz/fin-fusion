<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield('title', 'Fin-Fusion')</title>
    <link rel="stylesheet" href="{{ asset('assets/vendors/mdi/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/ti-icons/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/css/vendor.bundle.base.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="shortcut icon" href="{{ asset('assets/images/favicon.png') }}" />
    @yield('page-level-styles')
</head>
<body>
    @auth
        <div class="container-scroller">
            @include('backend.partials._navbar')
            <div class="container-fluid page-body-wrapper">
                @include('backend.partials._sidebar')
                <div class="main-panel">
                    <div class="content-wrapper">
                        @yield('main-content')
                    </div>
                    @include('backend.partials._footer')
                </div>
            </div>
        </div>
    @endauth

    @guest
        <div class="container-scroller">
            @if (!request()->routeIs('login') && !request()->routeIs('register') && !request()->routeIs('password.request'))
                @include('frontend.partials._navbar')
            @endif

            <div class="container-fluid page-body-wrapper full-page-wrapper">
                @if (!request()->routeIs('login') && !request()->routeIs('register') && !request()->routeIs('password.request'))
                    @include('frontend.partials._sidebar')
                @endif
                <div class="content-wrapper d-flex align-items-center auth">
                    @yield('main-content')
                    @if (!request()->routeIs('login') && !request()->routeIs('register') && !request()->routeIs('password.request'))
                        @include('frontend.partials._footer')
                    @endif
                </div>
            </div>
        </div>
    @endguest

    <script src="{{ asset('assets/vendors/js/vendor.bundle.base.js') }}"></script>

    <script src="{{ asset('assets/vendors/chart.js/chart.umd.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>

    <script src="{{ asset('assets/js/off-canvas.js') }}"></script>
    <script src="{{ asset('assets/js/misc.js') }}"></script>
    <script src="{{ asset('assets/js/settings.js') }}"></script>
    <script src="{{ asset('assets/js/todolist.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.cookie.js') }}"></script>

    <script src="{{ asset('assets/js/dashboard.js') }}"></script>
    @yield('page-level-scripts')

</body>

</html>
